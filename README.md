# Tab-Fader

Some websites are just too bright.

And some of them do not work (well) with [Dark Reader][0]. Or some big images
on dark webpages are too bright. It would be nice if we could just fade out
some websites.

There is already a similar plugin for Chrome: [Tab Brightness Control][1]. I
personally use Firefox so I can't use this plugin right now. There's also some
extensions for Firefox that do kinda similar things, but most of them don't
really work well or at all.

[0]: https://darkreader.org/
[1]: https://chrome.google.com/webstore/detail/tab-brightness-control/emdjkjjpeognjdnapljkfnihhgblekde


## Idea

The idea of Tab-Fader is to be able to dim the display brightness of
websites/pages that you configure it for.

So you would have a slider in each browser tab, that is by default all the way
up to full brightness. When you change it to 50%, the website's brightness also
becomes 50%. And this setting remains on this page, until you change it again.

It would also be nice to be able to configure it for a whole domain, subdomain,
subdirectory, regex, etc.

There should also be a global on/off switch so that you can turn it off when
you don't want any websites to fade.

And then it could also become time and location driven, so that during daylight
it automatically turns off.

But this is all just ideas, for now we just have this script, which already
does most of the work, but there's no UI yet. You can paste this script in the
developer console, and the website fades to 50%. You can change the opacity in
the script.

```js
const overlayDiv = document.createElement('div');
overlayDiv.style.position = 'fixed';
overlayDiv.style.top = 0;
overlayDiv.style.left = 0;
overlayDiv.style.width = '100%';
overlayDiv.style.height = '100%';
overlayDiv.style.zIndex = 999999;
overlayDiv.style.background = 'black';
overlayDiv.style.opacity = 0.5;
overlayDiv.style.pointerEvents = 'none';

const bodyEl = document.getElementsByTagName('body')[0];
bodyEl.appendChild(overlayDiv);
```

We could expand this script to inject a [slider][2] that controls the opacity.
Maybe we could even use local storage to save the setting. In this way we could
already get pretty far by just running a script in the developer console.

I'm not familiar with making browser extensions, so if anyone has the same
desire for this feature and can help out in this space then make sure to
contact me.

[2]: https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_rangeslider
